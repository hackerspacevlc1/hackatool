export class Requester {

  async get(url) {
    return await fetch(url, {
      // method: 'GET',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json',
        'Access-Control-Allow-Credentials': true
      }
    })
    .then( response => {
      if (!response.ok) { return console.log('[FRONT] Bad request') }

      return response.json()
    })
  }

  async post({url, data}) {
    return await fetch(url, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'content-type': 'application/json',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(data)
    })
    .then( response => {
      if (!response.ok) {
        // console.log('--------------', JSON.stringify(data))
       return console.log('[FRONT] Bad request', JSON.stringify(data))
      }

      return response
    })
  }
}
import { Requester } from "../domain/Requester"
import { redirect } from "../domain/router/Link"

const requester = new Requester()

async function handleLogin(event) {
    event.preventDefault()

    const data = Object.fromEntries(new window.FormData(event.target))
    const url = 'http://localhost:8000/v1/login/'
    
    // console.log('_#_#_#_', data)

    const token = await requester.post({url, data})

    if (token.ok) { redirect('/inventory') }
    
    return
}

export function LogIn () {

  return (
    <main>
      <h1>Log in</h1>

      <form onSubmit={handleLogin}>
        <input name='user_name' type="text" placeholder='email' defaultValue='diego'/>
        <input name='password' type="text" placeholder='password' defaultValue='123'/>
        <button type="submit">Log in</button>
      </form>
    </main>
  )
}
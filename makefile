build:
	docker compose build

init:
	rm -rf front/node_modules
	mkdir front/node_modules
	docker compose run --rm front npm install

up:
	docker compose up

down:
	docker compose down

front_install:
	docker compose run --rm front npm install

back_test:
	docker compose run --rm back ./manage.py test

migrate:
	docker compose run --rm back ./manage.py migrate

create_app:
	docker compose run --rm back ./manage.py startapp ${i}

scaffold:
	scaffold_django:
	scaffold_vite:

scaffold_django:
	docker compose run --rm django_scaffolder django-admin startproject back

scaffold_vite:
	docker compose run --rm vite_scaffolder npm create vite@4.4.0 ${i}


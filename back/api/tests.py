from django.test import TestCase, Client # type: ignore

class TestRequester(TestCase):
  c = Client()
  login_url = '/v1/login/'
  users_url = '/v1/users/'

  def test_creates_token_with_correct_credentials(self):
    credentials = {
      'user_name': 'diego',
      'password': '123',
    }
    
    response = self.c.post(self.login_url, credentials)

    self.assertEqual(response.status_code, 200)
    self.assertIn('session_token', response.cookies)
  
  def test_not_creates_token_with_incorrect_credentials(self):
    credentials = {
      'user_name': 'wrong name',
      'password': 'wrong password',
    }
    
    response = self.c.post(self.login_url, credentials)

    self.assertEqual(response.status_code, 401)
    self.assertNotIn('session_token', response.cookies)
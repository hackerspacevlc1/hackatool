from src.users_repository import UsersRepository

from django.shortcuts import HttpResponse # type: ignore
from rest_framework.decorators import api_view # type: ignore

from .authenticators.token_authenticator import TokenAuthenticator
from api.support.login_view import LoginView

import json
import sys

@api_view(['GET'])
def index(request):

    return HttpResponse("<h1>Hello World</h1>")

@api_view(['GET'])
def users(request):

    return HttpResponse(json.dumps(users))

@api_view(['POST'])
def login(request):

    return LoginView.create_response(request)
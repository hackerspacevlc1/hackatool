from django.utils.deprecation import MiddlewareMixin
from django.contrib.auth.models import User
from api.authenticators.token_authenticator import TokenAuthenticator
# from .jwt_utils import decode_access_token

class JWTAuthenticationMiddleware(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        # 'http://localhost:5173, http://localhost:5173'

        # response['Access-Control-Allow-Origin'] = "http://localhost:5173"
        # response['Access-Control-Allow-Origin'] = 'http://localhost:5173'
        # response['Access-Control-Allow-Headers'] = 'Content-Type: application/json'
        return response
    
    def process_request(self, request):

        token = request.headers.get('Authorization')
        
        if token:
            token = token.split('session_token=')[1]
            user_name = TokenAuthenticator.decode_token(token)
            if user_name:
                try:
                    user = User.objects.get(name=user_name)
                    request.user = user
                except User.DoesNotExist:
                    request.user = None
            else:
                request.user = None
        else:
            request.user = None
